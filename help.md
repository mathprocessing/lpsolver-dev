## Don't know how to add types for big-rational library

filename: `./types/big-rational.d.ts`

### Try 1
```ts
declare module 'big-rational' {
  export default class BigRat {
    public zero: BigRat;
    public one: BigRat;
  }
};
```

### Try 2 (just see to "big-integer" library into file `BigInteger.d.ts`)

```ts
declare module 'big-rational' {
  export = bigRat;
  export as namespace bigRat;
  declare var bigRat: bigRat.BigRatStatic;
  declare namespace bigRat {
    export default interface BigRatStatic {
      zero: number;
      (x: number);
    }
  }
}
```

# Import typedef from another file
https://github.com/jsdoc/jsdoc/issues/1537

# Integer type
https://spin.atomicobject.com/2018/11/05/using-an-int-type-in-typescript/
```ts
// or type Int = BigInt;
export type Int = number & { __int__: void };

export const roundToInt = (num: number): Int => Math.round(num) as Int;

export const toInt = (value: string): Int => {
  return Number.parseInt(value) as Int;
};

export const checkIsInt = (num: number): num is Int =>  num % 1 === 0;

export const assertAsInt = (num: number): Int => {
  try {
    if (checkIsInt(num)) {
      return num;
    }
  } catch (err) {
    throw new Error(`Invalid Int value (error): ${num}`);
  }

  throw new Error(`Invalid Int value: ${num}`);
};
```

# Bad code for r function (for learning purposes)
```js
// src/big-rat/index.js
function r(n, d = 1) {
  const mult = 10000000;
  if (typeof n === "number") {
    const intPart = ~~n;
    const floatPart = n - intPart;
    const intPartRat = new BigRational(bigInt(intPart));
    const floatPartRat = new BigRational bigInt((~~(floatPart * mult)), bigInt(mult));
    if (d === 1) {
      return intPartRat.add(floatPartRat);
    } else {
      return (intPartRat.add(floatPartRat)).div(bigInt(d));
    }
  } else
    if (bigInt.isInstance(n))
    {
      if (bigInt.isInstance(d)) {
        return n.div(d);
      } else if (typeof d === "number") {
        return n.div(r(d));
      }
    }
    else {
      throw new Error(`Unknown type ${typeof value}`);
    }
}
```