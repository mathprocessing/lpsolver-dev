# Example 1
```
maximize f = x1 + 2x2 + x3

2x1 + 7x2 + 6x3 <= 10 
-x1 + 6x2 + 5x3 <= 8 
4x1 +        x3 <= 12
x1,x2,x3 >= 0

answer:
f = 29/7
x1 = 3
x2 = 4/7 
x3 = 0
```

Let's solve `Example 1` for arbitrary coefficient `a` near variable `x2`

```
maximize f = x1 + a x2 + x3
answer: 
boundary = 0.95454555454545447767... (source: https://lovasoa.github.io/highs-js/)
boundary = 21/22 (source: main.js binarySearch + @todo implement nearestRational(x, dx, maxNumOrDenom))
(source: Wolfram Mathematica)
a < boundary => {f = 39/11,     {x1 -> 31/11, x2 -> 0,   x3 -> 8/11 }}
a > boundary => {f = 3 + 4/7 a, {x1 -> 3,     x2 -> 4/7, x3 -> 0    }}

(source: integer-solver.js)
a = 21/22 =>
solution type = TRUE
solution = [31/11, 0, 8/11, 0, 79/11, 0]
f = 39/11
...
  (source: manual calculations)
  3 + 4/7 * 21/22 = 39/11
  21/22 < 21/22 => {f = 39/11, {x1 -> 31/11, x2 -> 0,   x3 -> 8/11 }}
  21/22 > 21/22 => {f = 39/11, {x1 -> 3,     x2 -> 4/7, x3 -> 0    }}
...

A = [
  [ 2, 7, 6, 1, 0, 0],
  [-1, 6, 5, 0, 1, 0],
  [ 4, 0, 1, 0, 0, 1],
]

table = [                /*B*/
      [ 2, 7, 6, 1, 0, 0, 10],
      [-1, 6, 5, 0, 1, 0,  8],
      [ 4, 0, 1, 0, 0, 1, 12],
/*f*/ [-1,-2,-1, 0, 0, 0,  0],
]
```

# Example 2
```
maximize f = 9x1 + 10x2 + 16x3

2x1 + 7x2 + 6x3 <= 10 
-x1 + 6x2 + 5x3 <= 8 
4x1 +        x3 <= 12
x1,x2,x3 >= 0
```

make one iteration

```js
let minIndex = getMaxAboveZero(table[3])
console.log(minIndex)
```

```
index of column = getMinBelowZero(table[3]) = {index: 1, value: -2}
index of row    = minAboveZero([10/7, 8/6, 12/0]) = {index: 1, value: 8/6}

index of column = 1
index of row    = 1
table[1][1] = 6
```
TODO: continue `Example 2`

# Example 3

```
maximize f = 9x1 + 10x2 + 16x3

18x1 + 15x2 + 12x3 <= 360 
6x1  +  4x2 +  8x3 <= 192 
5x1  +  3x2 +  3x3 <= 180
x1,x2,x3 >= 0

-- iteration 1 --

  rows: [
    [18, 15, 12, 1, 0, 0, 360],
    [ 6,  4,  8, 0, 1, 0, 192],
    [ 5,  3,  3, 0, 0, 1, 180],
    [-9,-10,-16, 0, 0, 0,   0], // objective
  ],
  
basic plan: [0 0 0 360 192 180] f=0 
  plan is not optimal because exists negative element in objective row

getMinBelowZero = -16
replacing (row: 1, column: 2)

-- iteration 2 --

table = [
  [   9,   9,  0, 1,-3/2, 0,  72],
  [ 3/4, 1/2,  1, 0, 1/8, 0,  24],
  [11/4, 3/2,  0, 0,-3/8, 1, 108],
  [  3,   -2,  0, 0,   2, 0, 384],
]

basic plan: [0 0 24 72 0 108] f=384
  plan is not optimal because exists negative element in objective row

getMinBelowZero = -2
replacing (row: 0, column: 1)

-- iteration 3 --

table = [
  [   1,   1,  0,   1/9,  -1/6, 0,   8],
  [ 1/4,  -0,  1, -1/18,  5/24, 0,  20],
  [ 5/4,  -0,  0,  -1/6,  -1/8, 1,  96],
  [   5,   0,  0,   2/9,   5/3, 0, 400],
]

basic plan: [0 8 20 0 0 96] f=400 
  plan is optimal

answer: {400, {x1 -> 0, x2 -> 8, x3 -> 20}}
```
# Example 4
+ [Video part 1](https://www.youtube.com/watch?v=iwDiG2mR6FM)
+ [Video part 2](https://www.youtube.com/watch?v=woJAb5EgjtI)
+ [DUAL SIMPLEX METHOD](https://www.youtube.com/watch?v=KLHWtBpPbEc)

```text
maximize f = 8x1 + 10x2 + 7x3
x1 + 3x2 + 2x3 <= 10
x1 + 5x2 + x3 <= 8
x1,x2,x3 >= 0
```

# Example 5

```js
floatTable = {
  basicOrder: [3, 4, 5], // order of basic variables
  rows: [
    [-2,  1, -1, 1, 0, 0, -4],
    [ 1,  1,  2, 0, 1, 0,  8],
    [ 0, -1,  1, 0, 0, 1, -2],
    [+1, +2, +3, 0, 0, 0,  0], // objective row
  ],
  solutionExist: solutionType.UNDEFINED,
}
```

optimal basic plan:
1. `[    3,    2,    0,    0,    3,    0]`
* f = -7

# Example 5 (reversed)
```js
floatTable = {
  basicOrder: [0, 1, /*4 or 5*/], // order of basic variables
  rows: [
    [-2,  1, -1, 1, 0, 0, -4],
    [ 1,  1,  2, 0, 1, 0,  8],
    [ 0, -1,  1, 0, 0, 1, -2],
    [-1, -2, -3, 0, 0, 0,  0], // objective row
  ],
  solutionExist: solutionType.UNDEFINED,
}
```

have several solutions:
1. `[    4,    4,    0,    0,    0,    2]`
2. `[    3,    3,    1,    0,    0,    0]`
* f = 12

# Other
```js
floatTable = {
  basicOrder: [1], // порядок записи базисных переменных
  rows: [
    [1, 1, 1],
    [1, 2, 2], // objective
  ],
  solutionExist: solutionType.UNDEFINED,
}
// <=>
// x + y = 1
// x + 2y + f = 2, f -> max

floatTable = {
  basicOrder: [3, 4, 5], // порядок записи базисных переменных
  vars: [0, 1, 2, 3, 4, 5], // порядок записи переменных
  rows: [
    [18, 15, 12, 1, 0, 0, 360],
    [ 6,  4,  8, 0, 1, 0, 192],
    [ 5,  3,  3, 0, 0, 1, 180],
    [-9,-10,-16, 0, 0, 0,   0], // objective
  ],
  solutionExist: solutionType.UNDEFINED,
}

floatTable = {
  basicOrder: [1], // порядок записи базисных переменных
  rows: [
    [3, 1, 1],
    [1, 1, 0], // objective
  ],
  solutionExist: solutionType.UNDEFINED,
}
```

```text
LPSolve IDE:
max: x1 + 2x2 - 4x3 -3x4;
x1 + x2 <= 5;
2x1 - x2 >= 0;
-x1 + 3x2 >= 0;
x3 + x4 >= .5;
x3 >= 1.1;
x3 <= 10;
[5/3, 10/3, 11/10, 0, 0, 0, 25/3, 3/5, 0, 89/10]
f = 59/15
free x2, x4; => x4 := s4 - s5, s4>0, s5>0
[5/3, 10/3, 11/10, -3/5]
f = 86/15

Note: Free variables can be less than zero
```

```js
floatTable = {
    // x1 x2 x3 x4
  freeVars: [0, 1, 0, 1, 0,0,0,0,0,0],
  basicOrder: [5,6,7,8,9,10], // порядок записи базисных переменных
  rows: [ // 1 для свободной + 6 дополнительных переменных, так как 6 неравенств
    [ 1, 1, 0, 0,  0, 1,0,0,0,0,0,    5], //x1 + x2 <= 5
    [-2, 1, 0, 0,  0, 0,1,0,0,0,0,    0], //-2x1 + x2 <= 0
    [ 1,-3, 0, 0,  0, 0,0,1,0,0,0,    0], //+x1 - 3x2 <= 0
    [ 0, 0,-1,-1,  1, 0,0,0,1,0,0, -0.5], // -x3-x4+x5 <= -0.5
    [ 0, 0,-1, 0,  0, 0,0,0,0,1,0, -1.1], // -x3 <= -1.1
    [ 0, 0, 1, 0,  0, 0,0,0,0,0,1,   10], // x3 <= 10
    [-1,-2, 4, 3, -3, 0,0,0,0,0,0,    0], // -x1 - 2x2 + 4x3 + 3x4 - 3x5 + f = 0, f -> maximize
  ],
  solutionExist: solutionType.UNDEFINED,
}
```
