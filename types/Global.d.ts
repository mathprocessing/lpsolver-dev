/*
Bad fix: @typedef {number | bigInt.BigInteger | BigRational} AnyNumber
*/

interface Table<T> {
  basicOrder: number[];
  rows: T[][];
  solutionExist: solutionType;
  isRational: boolean;
}

interface Copyable {
  copy(): Copyable;
}

interface Solution<T> {
  f: T;
  solution: T[];
  solutionExist: solutionType;
}
