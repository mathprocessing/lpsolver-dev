const bigInt = require('big-integer');
const { BigRational, r } = require("./my-big-rat");
const { testSolver, findSolution, printSolution } = require('./integer-solver');
const { solutionType } = require('./consts');
const { justifyStringTo, tableToRational } = require('./rational-utils');
const { assert, CC } = require('./utils');
// testSolver();

// const floatTable = {
//   basicOrder: [0, 1, 4], // order of basic variables
//   rows: [
//     [-2,  1, -1, 1, 0, 0, -4],
//     [ 1,  2,  2, 0, 1, 0,  8],
//     [ 0, -1,  1, 0, 0, 1, -2],
//     [-1, -2, -3, 0, 0, 0, 0], // objective row
//   ],
//   solutionExist: solutionType.UNDEFINED,
//   isRational: false,
// }

/**
 * @param {import('./my-big-rat').AnyNumber} n
 * @returns {string} type of number
 */
function getNumberType(n) {
  if (typeof n === "number") {
    return "number";
  } else if (bigInt.isInstance(n)) {
    return "bigInt";
  } else if (BigRational.isInstance(n)) {
    return "BigRational";
  } else {
    throw new Error("n have unknown number type");
  }
}

/**
 * Solve task from `Example 1` in `examples.md`
 * @param {BigRational} a 
 * @returns {boolean} `x2 <= 0`
 */
function solveForCoef(a) {
  console.log(`\na = ${a}`)
  assert(a instanceof BigRational);
  const floatTable = {
    basicOrder: [0, 1, 2], // order of basic variables
    rows: [
      [ 2, 7, 6, 1, 0, 0, 10],
      [-1, 6, 5, 0, 1, 0,  8],
      [ 4, 0, 1, 0, 0, 1, 12],
      [-1, a.negate(),-1, 0, 0, 0,  0], // f
    ],
    solutionExist: solutionType.UNDEFINED,
    isRational: false,
  }

  const sol = findSolution(tableToRational(floatTable), 8);
  printSolution(sol);
  const x2 = sol.solution[2];
  console.log("Type of sol.solution[2]:", CC.setGreen(getNumberType(x2)));
  return !x2.gt(r(0)); // !(x2 > 0)
}

const minErr = r(1, 1e20);
const analyticAnswer = r(21, 22); // a = 21/22
let iteration = 0;

/**
 * 
 * @param {(_: BigRational) => boolean} f
 * @param {BigRational} x 
 * @param {BigRational} dx 
 * @returns {BigRational} parameter - result of search
 */
function binSearch(f, x, dx) {
  assert(analyticAnswer.lt(x.add(dx)) && analyticAnswer.gt(x));

  console.log(`iteration = ${++iteration}, parameter = ${x}, dx = ${dx}`);
  if (dx.lt(minErr)) {
    return x;
  }
  // Assumptions:
  // f(x) = false
  // f(x + dx) = true
  const halfdx = dx.mul(r(1, 2));
  const value = f(x.add(halfdx));
  if (value) {
    // go left interval
    return binSearch(f, x, halfdx);
  } else {
    // go right interval
    return binSearch(f, x.add(halfdx), halfdx);
  }
}

// console.log(solveForCoef(r(0.9545)));
// console.log(solveForCoef(r(0.9546)));

// TODO: move this to test-integer-solver.js
const answer = binSearch(solveForCoef, r(0.95), r(0.1));
console.log("Answer: ");
console.log(answer.toString());
console.log(answer.toDecimal(30));

console.log(CC.setYellow("\n*** Solution for `a = 21/22` ***"));
const answer2 = solveForCoef(analyticAnswer);
console.log("answer2:", answer2);