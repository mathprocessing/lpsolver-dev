const NOT_FOUND = -1;

const solutionType = {
  FALSE: 0,
  TRUE: 1,
  UNDEFINED: 2,
  NOT_BOUND: 3,
  NEGATIVE: 4,
  0: "FALSE",
  1: "TRUE",
  2: "UNDEFINED",
  3: "NOT_BOUND",
  4: "NEGATIVE",
};

module.exports = {
  solutionType,
  NOT_FOUND,
};
