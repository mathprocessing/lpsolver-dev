const bigInt = require("big-integer");
const { BigRational, r } = require("./my-big-rat");
const DEBUG_DIGITS = 0;

/**
 * @typedef {import('./my-big-rat').AnyNumber} AnyNumber
 */

const {
  solutionExistingToString,
  copyTable,
} = require('./utils');

/**
 * @param {any} obj 
 * @returns boolean
 */
function isBigRat(obj) {
  return obj instanceof BigRational;
}

/**
 * Compare two rational numbers
 * ```
 * greater(-inf, -inf) = false A1 
 * greater(-inf, +inf) = false A1 A2
 * greater(+inf, -inf) = true        A3
 * greater(+inf, +inf) = false    A2
 * greater(   x, -inf) = true           A4
 * 
 * A1. greater(-inf, y) = false
 *     x != -inf
 * A2. greater(x, +inf) = false
 *     y != +inf
 * A3. greater(+inf, y) = true
 *     -inf < x < +inf
 * A4. greater(x, -inf) = true
 * ```
 * @param {BigRational | number } ratX
 * @param {BigRational | number } ratY
 * @returns {boolean}
 */
function greater(ratX, ratY) {
  if (ratX === -Infinity || ratY === +Infinity) {
    return false;
  } else // ratY !== +Infinity && ratX !== -Infinity
    if (ratX === +Infinity || ratY === -Infinity) {
      return true;
    } else {
      if (ratX instanceof BigRational && ratY instanceof BigRational) {
        return ratX.greater(ratY);
      } else {
        throw new Error("one of ratX or ratY is not a BigRational");
      }
    }
}

/**
 * Compare two rational numbers
 * @param {BigRational | number} ratX 
 * @param {BigRational | number} ratY 
 * @returns {boolean}
 */
function lesser(ratX, ratY) {
  if (ratX === +Infinity || ratY === -Infinity) {
    return false;
  } else // ratY != -Infinity && ratX != +Infinity
    if (ratX === -Infinity || ratY === +Infinity) {
      return true;
    } else {
      if (ratX instanceof BigRational && ratY instanceof BigRational) {
        return ratX.lesser(ratY);
      } else {
        throw new Error("one of ratX or ratY is not a BigRational");
      }
    }
}

/**
 * Convert `any number` table to `BigRational` table
 * @param {Table<AnyNumber>} table 
 * @returns {Table<BigRational>}
 */
function tableToRational(table) {
  for (let i = 0; i < table.rows.length; i++) {
    for (let j = 0; j < table.rows[i].length; j++) {
      table.rows[i][j] = BigRational.fromAny(table.rows[i][j]);
    }
  }
  table.isRational = true; // fix bug with float number in solutionObject
  // @ts-ignore
  return table;
}

/**
 * Convert float, rational or bigInt to String
 * @param {AnyNumber} rational 
 * @param {number} numberLength 
 * @returns {string}
 */
function rationalToString(rational, numberLength = DEBUG_DIGITS) {
  return justifyStringTo(rational.toString(), numberLength);
}

/**
 * Add char to beginning of string `s` with goal to `s.length >= targetLength`
 * @param {string} s 
 * @param {number} targetLength
 * @returns string with `length >= targetLength:`
 * 
 * more delailed:
 * 
 * `if inputLength <= targetLength ==> s.length = targetLength`
 * 
 * `if inputLength > targetLength ==> s.length = inputLength`
 */
function justifyStringTo(s, targetLength, ch = ' ') {
  while (s.length < targetLength) {
    s = ch + s;
  }
  return s;
}

/**
 * Pretty print simplex table
 * @param {Table<number | BigRational>} table
 * @param {number} numberLength 
 */
function printRationalTable(table, numberLength = DEBUG_DIGITS) {
  console.log('Table: {');
  console.log(`basicOrder:`, table.basicOrder);
  for (let i = 0; i < table.rows.length; i++) {
    console.log(`r${i}:`, rationalArrayToString(table.rows[i], numberLength));
  }
  console.log(`solutionExist:`, solutionExistingToString(table.solutionExist), '\n}');
}

/**
 * 
 * @param {(number | BigRational)[]} rArray 
 * @param {number} numberLength 
 * @returns 
 */
function rationalArrayToString(rArray, numberLength = DEBUG_DIGITS) {
  let s = '[';
  for (let i = 0; i < rArray.length; i++) {
    if (i > 0) {
      s += ',';
    }
    s += rationalToString(rArray[i], numberLength);
  }
  s += ']';
  return s;
}

/**
 * Convert array of floating point numbers to array of rational numbers
 * @param {number[]} arr 
 * @returns {BigRational[]} array of `BigRational`
 */
function arrayToRational(arr) {
  return arr.map(r);
}

module.exports = {
  tableToRational,
  rationalToString,
  justifyStringTo,
  printRationalTable,
  rationalArrayToString,
  arrayToRational,
  greater,
  lesser,
  isBigRat,
};
