const bigInt = require('big-integer');

/**
 * typedef {number | bigInt.BigInteger | BigRational} AnyNumber
 */

/**
 * @typedef {import("./util-types").AnyNumber} AnyNumber
 */

class BigRational {
  /**
   * 
   * @param {bigInt.BigInteger} num 
   * @param {bigInt.BigInteger} denom
   */
  constructor(num, denom = bigInt[1], selfReducing = true) {
    // Alias properties kept for backwards compatability
    if (denom.isZero()) throw "Denominator cannot be 0.";
    this.numerator = this.num = num;
    this.denominator = this.denom = denom;
    if (selfReducing && !denom.eq(bigInt(1))) {
      this.selfReduce();
    }
  }

  /**
   * @param {*} x 
   * @returns {x is BigRational}
   */
  static isInstance = x => x instanceof BigRational;

  [Symbol.for('nodejs.util.inspect.custom')]() {
    return this.toString();
  }

  /**
   * Get `BigRational` from `any number`
   * @param {AnyNumber} n 
   * @returns {BigRational}
   */
  static fromAny(n) {
    if (typeof n === "number") {
      return r(n);
    } else if (BigRational.isInstance(n)) {
        return n.copy();
    } else if (bigInt.isInstance(n)) {
      // instanceof bigInt is actually bigInt | BigRational ==> use bigInt.isInstance(any)
      return new BigRational(n);
    } else {
      throw new Error("Unknown type " + typeof n);
    }
  }

  /**
   * @param {number} n 
   * @returns 
   */
  static fromFloat(n) {
    return BigRational.parseDecimal(n.toString());
  }

  /**
   * @param {bigInt.BigInteger} value 
   */
  setNumerator(value) {
    this.numerator = this.num = value;
  }

  /**
   * @param {bigInt.BigInteger} value 
   */
  setDenominator(value) {
    this.denominator = this.denom = value;
  }
  /**
   * Copy instance of `BigRational` class
   * @returns Copy of instance
   */
  copy() {
    return new BigRational(this.num, this.denom);
  }

  selfReduce() {
    const reduced = BigRational.reduce(this.num, this.denom);
    this.setNumerator(reduced.num);
    this.setDenominator(reduced.denom);
  }

  /**
   * 
   * @param {bigInt.BigInteger} n 
   * @param {bigInt.BigInteger} d 
   * @returns 
   */
  static reduce(n, d) {
    if (!(bigInt.isInstance(n) && bigInt.isInstance(d))) {
      throw new Error("reduce: only bigInt input args supported");
    }
    const divisor = bigInt.gcd(n, d);
    const num = n.over(divisor);
    const denom = d.over(divisor);
    if (denom.isNegative()) {
        return new BigRational(num.negate(), denom.negate(), false);
    }
    return new BigRational(num, denom, false);
  }

  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  add(v) {
    const multiple = bigInt.lcm(this.denom, v.denom);
    let a = multiple.divide(this.denom);
    let b = multiple.divide(v.denom);

    a = this.num.times(a);
    b = v.num.times(b);
    return BigRational.reduce(a.add(b), multiple);
  }

  /**
   * Substract from this other rational number
   * @param {BigRational} v
   * @returns {BigRational}
   */
  sub(v) {
    return this.add(v.negate());
  }


  /**
   * Multiply this to other rational number
   * @param {BigRational} v
   * @returns {BigRational}
   */
  mul(v, reduceAfter = true) {
    if (reduceAfter) {
      // not mutable
      return BigRational.reduce(this.num.times(v.num), this.denom.times(v.denom));
    } else {
      // mutable
      this.num.times(v.num);
      this.denom.times(v.denom);
      return this;
    }
  }

  /**
   * Divide two numbers
   * @param {BigRational} v
   * @returns 
   */
  div(v, reduceAfter = true) {
    if (reduceAfter) {
      return BigRational.reduce(this.num.times(v.denom), this.denom.times(v.num));
    } else {
      this.num.times(v.denom);
      this.denom.times(v.num);
      return this;
    }
  }

  reciprocate() {
    return new BigRational(this.denom, this.num);
  }

  /**
   * Modulus operator (like %)
   * @param {BigRational} v
   * @returns 
   */
  mod(v) {
    const q = this.div(v).floor();
    return this.sub(v.mul(q));
  };
  
  /**
   * @param {number} n 
   * @returns 
   */
  pow(n) {
    const v = bigInt(n);
    const num = this.num.pow(v);
    const denom = this.denom.pow(v);
    return BigRational.reduce(num, denom);
  };
  
  /**
   * see `Math.floor`
   * @returns {BigRational}
   */
  floor() {
    const divmod = this.num.divmod(this.denom);
    let floor;
    if (divmod.remainder.isZero() || !divmod.quotient.isNegative()) {
      floor = divmod.quotient;
    } else {
      floor = divmod.quotient.prev();
    }
    return new BigRational(floor);
  };
  
  /**
   * see `Math.ceil`
   * @param {boolean} toBigInt 
   * @returns 
   */
  ceil(toBigInt) {
    var divmod = this.num.divmod(this.denom),
        ceil;
    if (divmod.remainder.isZero() || divmod.quotient.isNegative()) {
        ceil = divmod.quotient;
    }
    else ceil = divmod.quotient.next();
    if (toBigInt) return ceil;
    return new BigRational(ceil);
  };
  
  /**
   * see `Math.round`
   * @returns 
   */
  round() {
    return this.add(new BigRational(bigInt[1], bigInt[2])).floor();
  };
  
  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  compareAbs(v) {
    if (this.denom.equals(v.denom)) {
        return this.num.compareAbs(v.num);
    }
    return this.num.times(v.denom).compareAbs(v.num.times(this.denom));
  };
  
  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  compare(v) {
    if (this.denom.equals(v.denom)) {
        return this.num.compare(v.num);
    }
    var comparison = this.denom.isNegative() === v.denom.isNegative() ? 1 : -1;
    return comparison * this.num.times(v.denom).compare(v.num.times(this.denom));
  };
  
  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  equals(v) {
    return this.compare(v) === 0;
  }

  eq = this.equals;
  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  notEquals(v) {
    return !this.equals(v);
  }

  neq = this.notEquals;

  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  lesser(v) {
    return this.compare(v) < 0;
  }

  lt = this.lesser;

  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  lesserOrEquals(v) {
    return this.compare(v) <= 0;
  }

  leq = this.lesserOrEquals;

  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  greater(v) {
    return this.compare(v) > 0;
  }

  gt = this.greater;

  /**
   * 
   * @param {BigRational} v
   * @returns 
   */
  greaterOrEquals(v) {
    return this.compare(v) >= 0;
  }

  geq = this.greaterOrEquals;

  abs() {
    if (this.isPositive()) return this;
    return this.negate();
  }

  negate() {
    if (this.denom.isNegative()) {
        return new BigRational(this.num, this.denom.negate());
    }
    return new BigRational(this.num.negate(), this.denom);
  }

  isNegative() {
    return this.num.isNegative() !== this.denom.isNegative() && !this.num.isZero();
  }

  isPositive() {
    return this.num.isNegative() === this.denom.isNegative() && !this.num.isZero();
  }

  isZero() {
    return this.num.isZero();
  }

  /**
   * 
   * @param {number} digits 
   * @returns 
   */
  toDecimal(digits = 10) {
    const n = this.num.divmod(this.denom);
    let intPart = n.quotient.abs().toString();
    const remainder = BigRational.parse(n.remainder.abs(), this.denom);
    const shiftedRemainder = remainder.mul(new BigRational(bigInt("1e" + digits)));
    let decPart = shiftedRemainder.num.over(this.denom.abs()).toString();
    if (decPart.length < digits) {
        decPart = new Array(digits - decPart.length + 1).join("0") + decPart;
    }
    if (shiftedRemainder.num.mod(shiftedRemainder.denom).isZero()) {
        while (decPart.slice(-1) === "0") {
            decPart = decPart.slice(0, -1);
        }
    }
    if (digits < 1) decPart = "";
    if (this.isNegative()) {
        intPart = "-" + intPart;
    }
    if (decPart === "") {
        return intPart;
    }
    return intPart + "." + decPart;
  }

  toString() {
    if (this.denom.equals(bigInt[1])) {
      return String(this.num);
    } else
    return String(this.num) + "/" + String(this.denom);
  }

  valueOf() {
    if (!isFinite(+this.num) || !isFinite(+this.denom)) {
        return +this.toDecimal(64);
    }
    return this.num.divide(this.denom);
  }

  /**
   * 
   * @param {String} n 
   * @returns {BigRational}
   */
  static parseDecimal(n) {
    let parts = n.split(/e/i);
    if (parts.length > 2) {
        throw new Error("Invalid input: too many 'e' tokens");
    }
    if (parts.length > 1) {
        let isPositive = true;
        if (parts[1][0] === "-") {
            parts[1] = parts[1].slice(1);
            isPositive = false;
        }
        if (parts[1][0] === "+") {
            parts[1] = parts[1].slice(1);
        }
        const significand = BigRational.parseDecimal(parts[0]);
        const exponent = new BigRational(bigInt[10].pow(parts[1]), bigInt[1]);
        if (isPositive) {
            return significand.mul(exponent);
        } else {
            return significand.div(exponent);
        }
    }

    parts = n.trim().split(".");
    if (parts.length > 2) {
        throw new Error("Invalid input: too many '.' tokens");
    }
    if (parts.length > 1) {
        const isNegative = parts[0][0] === '-';
        if (isNegative) parts[0] = parts[0].slice(1);
        let intPart = new BigRational(bigInt(parts[0]), bigInt[1]);
        const length = parts[1].length;
        while (parts[1][0] === "0") {
            parts[1] = parts[1].slice(1);
        }
        var exp = "1" + Array(length + 1).join("0");
        var decPart = BigRational.reduce(bigInt(parts[1]), bigInt(exp));
        intPart = intPart.add(decPart);
        if (isNegative) intPart = intPart.negate();
        return intPart;
    }
    return new BigRational(bigInt(n));
  }

  /**
   * create `BigRational` from float or rational
   * @param {AnyNumber} a 
   * @param {AnyNumber} b 
   * @returns {BigRational}
   */
  static parse(a, b) {
    if (a === 0) {
      return new BigRational(bigInt[0], bigInt[1]);
    }
    if (b !== 0 && typeof a === "number" && typeof b === "number") {
      return BigRational.reduce(bigInt(a), bigInt(b));
    }
    if (bigInt.isInstance(a)) {
        return new BigRational(a, bigInt[1]);
    }
    if (BigRational.isInstance(a)) {
      if (typeof b === "number") {
        if (b === 1) {
          return a;
        } else {
          throw new Error("Not supported type 'number' for denominator");
        }
      } else if (bigInt.isInstance(b)) {
        if (b.eq(bigInt[1])) {
          return a;
        } else {
          return a.div(new BigRational(b));
        }
      } 
    }

    if (typeof a !== "number") {
      throw new Error("Can't happens");
    }

    // typeof a === "number" && (typeof b !== "number" || b === 0)
    if (b === 0) {
      throw new Error("Zero division exception");
    }

    // typeof a === "number" && typeof b !== "number"

    if (BigRational.isInstance(b)) {
      throw new Error("Not supported type 'BigRational' for denominator");
    }

    // typeof a === "number" && bigInt.isInstance(b)

    if (bigInt.isInstance(b)) {
        if (!b.eq(bigInt[1])) {
          throw new Error("Not supported type 'bigInt' for denominator");
        }
    }

    // Assumption bigInt.isInstance(b), b === bigInt[1]

    var num;
    var denom;

    var text = String(a);
    var texts = text.split("/");
    if (texts.length > 2) {
        throw new Error("Invalid input: too many '/' tokens");
    }
    if (texts.length > 1) {
        var parts = texts[0].split("_");
        if (parts.length > 2) {
            throw new Error("Invalid input: too many '_' tokens");
        }
        if (parts.length > 1) {
            var isPositive = parts[0][0] !== "-";
            num = bigInt(parts[0]).times(texts[1]);
            if (isPositive) {
                num = num.add(parts[1]);
            } else {
                num = num.subtract(parts[1]);
            }
            denom = bigInt(texts[1]);
            return BigRational.reduce(num, denom);
        }
        return BigRational.reduce(bigInt(texts[0]), bigInt(texts[1]));
    }
    return BigRational.parseDecimal(text);
  }

  interpret = BigRational.parse;
}

/**
 * Create instance of BigRational `n / d` from two float numbers
 * 
 * Properties: 
 * - `r(x) === r(x, 1)`
 * 
 * Example:
 * ```
 * r(-0.5) // -1/2
 * r(-0.5, 1) // -1/2
 * r(3, 2) // returns new BigRational(bigInt(3), bigInt(2))
 * r(3.5, 2.5) // returns BigRational.fromFloat(3.5 / 2.5)
 * 
 * ```
 * @param {number} n numerator
 * @param {number} d denominator
 * @returns {BigRational} Rational number
 */
 function r(n, d = 1) {
  const floatPart = n - (~~n);
  if (floatPart === 0) {
    return BigRational.parse(n, d);
  } else {
    if (typeof d !== "number") {
      throw new Error('typeof d !== "number"');
    }
    if (d === 1) {
      return BigRational.fromFloat(n);
    } else {
      return BigRational.fromFloat(n / d);
    }
  }
}

module.exports = {
  BigRational, r
};
