import { BigRational } from './'; // from index.js
export type AnyNumber = number | bigInt.BigInteger | BigRational;