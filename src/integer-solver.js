// необходимо использовать typescript для жёсткой типизации
// вместо этого временно используем Object.hasOwnProperty
const { BigRational, r } = require("./my-big-rat");
// список функций библиотеки big-rational:
// console.log(r(1).__proto__)

/**
 * @typedef {import('./my-big-rat').AnyNumber} AnyNumber
 */

//---------------------------------
const MAX_STEPS = 8;
const DEBUG_RATIONAL_DIGITS = 5;
//---------------------------------
const {
  solutionType,
  NOT_FOUND,
} = require('./consts');

const {
  copyTable,
  getSolutionVector,
  assert,
  CC,
} = require('./utils');

const {
  tableToRational,
  rationalToString,
  printRationalTable,
  rationalArrayToString,
  greater,
  lesser,
} = require('./rational-utils');

/**
 * @param {BigRational[]} inputArray 
 * @returns {number} integer index of minimal number from set `numbers below zero` except last element of input array
 */
function getMinBelowZeroExceptLast(inputArray) {
  let minIndex = NOT_FOUND, min = Infinity;
  for (let i = 0; i < inputArray.length - 1; i++) {
    //               except last element
    /** @type BigRational */
    let elem = inputArray[i];
    if (lesser(elem, r(0)) && lesser(elem, min)) {
      // @ts-ignore
      min = elem;
      minIndex = i;
    }
  }
  return minIndex;
}

/**
 * 
 * @param {BigRational[][]} rows 
 * @param {number} columnIndex 
 * @param {number} Bindex 
 * @returns {number} minimal row index
 */
function getRowIndex(rows, columnIndex, Bindex) {
  /** @type {BigRational | number} */
  let min = Infinity;
  let minIndex = NOT_FOUND;
  for (let i = 0; i < rows.length - 1; i++) {
    //               except last row
    let row = rows[i];
    let keyNumber = row[columnIndex];
    // greater(lesser) работает не только с bigrational, но и c +-Infinity
    if (greater(keyNumber, r(0))) { // row[columnIndex] > 0
      // rowQ = rowB / mainRow
      let elem = row[Bindex].div(keyNumber);
      if (lesser(elem, min)) { // elem < min
        /** @type BigRational */
        min = elem;
        minIndex = i;
      }
    }
  }
  return minIndex;
}

/**
 * Check that table have negative solution.
 * @param {Table<BigRational>} table 
 * @param {number} Bindex 
 */
function hasNegativeSolution(table, Bindex) {
  for (let i = 0; i < table.rows.length - 1; i++) {
    // get element from column B
    let solutionElement = table.rows[i][Bindex];

    if (lesser(solutionElement, r(0))) {
      return true;
    }
  }
  return false;
}

/**
 * 
 * @param {Table<BigRational>} table 
 * @returns 
 */
function nextStep(table) {
  let rows = table.rows;
  let objectiveRow = rows[rows.length - 1];
  let Bindex = rows[0].length - 1; // or Bindex = table.vars.length

  if (hasNegativeSolution(table, Bindex)) {
    table.solutionExist = solutionType.NEGATIVE;
    return table;
  }

  let columnIndex = getMinBelowZeroExceptLast(objectiveRow);

  if (columnIndex == NOT_FOUND) {
    // тут ошибка(была) не lastObjectiveElement < 0 а искусственная переменная в методе МИБ должна быть < 0
    // lastObjectiveElement >= 0
    // solution founded => exit algorithm
    table.solutionExist = solutionType.TRUE;
    return table;
  }
  // columnIndex != NOT_FOUND
  let rowIndex = getRowIndex(rows, columnIndex, Bindex);

  if (rowIndex == NOT_FOUND) {
    table.solutionExist = solutionType.NOT_BOUND;
    return table;
  }
  // console.log(`rowIndex = ${rowIndex}`);
  // console.log(`columnIndex = ${columnIndex}`);
  // console.log('q = ');
  // for (let i = 0; i < rows.length - 1; i++) {
  //   if (rows[i][columnIndex].isZero()) {
  //     console.log(`(${rows[i][Bindex]}) / (${rows[i][columnIndex]}) = +-Infinity`);
  //   } else {
  //     console.log(`(${rows[i][Bindex]}) / (${rows[i][columnIndex]}) = ${rows[i][Bindex].div(rows[i][columnIndex])}`);
  //   }
  // }
  // console.log(`intersectElement = ${rows[rowIndex][columnIndex]}`);
  const newTable = gaussElimination(table, rowIndex, columnIndex);
  newTable.basicOrder[rowIndex] = columnIndex;
  return newTable;
}

/**
 * Check for property of elements in table: if `property === false` throws `Error("Wrong property for table element")`
 * @param {(elem: AnyNumber) => boolean} property 
 * @param {Table<AnyNumber>} table
 */
function checkPropOfTableElements(property, table) {
  assert(table.isRational === true, CC.setYellow(`isRational = ${table.isRational}`));
  for (let i = 0; i < table.rows.length; i++) {
    for (let j = 0; j < table.rows[i].length; j++) {
      const elem = table.rows[i][j];
      if(!property(elem)) {
        console.log(table);
        console.log(CC.setGreen(`Wrong property for table element table.rows[${i}][${j}] = ${elem}`));
        throw new Error("Wrong property for table element");
      }
    }
  }
}

/**
 * 
 * @param {Table<BigRational>} table 
 * @param {number} rowIndex 
 * @param {number} columnIndex 
 * @returns {Table<BigRational>}
 */
function gaussElimination(table, rowIndex, columnIndex) {
  // метод прямоугольников
  const rows = table.rows;
  const newTable = copyTable(table);

  const a1 = rows[rowIndex][columnIndex];
  // console.log(`\ngaussElimination: `);
  // console.log(`a1 = ${rationalToString(a1)}`);
  if (lesser(a1, r(0))) {
    // throw new Error(`a1 <= 0`)
    // console.log(`--- a1 < 0 ---`);
  }
  if (a1.equals(r(0))) {
    throw new Error(`a1 = 0`);
  }
  // i = rowIndex, j = columnIndex
  for (let i = 0; i < rows.length/*all rows*/; i++) {
    const row = rows[i];
    // console.log()
    if (i == rowIndex) {
      // newRow = row * (1/a1)
      // console.log(`i == rowIndex`)
      for (let j = 0; j < row.length; j++) {
        const b2 = row[j]; // b2 и a1 имеют тип bigrational
        const newElement = b2.div(a1);
        newTable.rows[i][j] = newElement;
        // console.log(`(r${i}, c${j}) = (${rationalToString(b2)}) / (${rationalToString(a1)}) = ${rationalToString(newElement)}`)
      }
    } else {
      // newRow = row - (coef * mainRow)
      const b1 = row[columnIndex]; // bigrational
      const coef = b1.div(a1); // bigrational and a1 > 0
      // console.log(`coef = (${rationalToString(b1)}) / (${rationalToString(a1)}) = ${rationalToString(coef)}`)

      for (let j = 0; j < row.length; j++) {
        const b2 = row[j];
        const a2 = rows[rowIndex][j];
        const newElement = b2.sub(coef.mul(a2));
        newTable.rows[i][j] = newElement;
        // console.log(`(r${i}, c${j}) = ${rationalToString(b2)} - (${rationalToString(coef)}) * ${rationalToString(a2)} = ${rationalToString(newElement)}`)
      }
    }
  }
  //@ts-ignore
  return newTable;
}

/**
 * Get solution object from given table.
 * @template T
 * @param {Table<T>} table
 * @returns {Solution<T>}
 */
 function getSolutionObject(table) {
  const solutionVector = getSolutionVector(table);
  const objectiveRow = table.rows[table.rows.length - 1];
  const lastElement = objectiveRow[objectiveRow.length - 1];
  return { f: lastElement, solution: solutionVector, solutionExist: table.solutionExist };
}

/**
 * Print solution for given table.
 * @param {Table<BigRational | number>} table 
 */
function printSolutionFromTable(table) {
  printSolution(getSolutionObject(table));
}

/**
 * Print solution for given table.
 * @param {Solution<BigRational | number>} solution
 */
function printSolution({ f, solution, solutionExist }) {
   // @ts-ignore
  console.log(`solution type = ${solutionType[solutionExist]}`);
  console.log(`solution = ${rationalArrayToString(solution, DEBUG_RATIONAL_DIGITS)}`);
  console.log(`f = ${rationalToString(f)}`);
}


/**
 * 
 * @param {Table<BigRational>} table 
 * @returns {Table<BigRational>}
 */
function setZeroObjectiveRowInBasicColumns(table) {
  for (let i = 0; i < table.basicOrder.length; i++) {
    let varIndex = table.basicOrder[i];
    if (true/* objectiveRow[varIndex] != 0 */) {
      table = gaussElimination(table, i, varIndex);
    }
  }
  return table;
}

// ***NOT FEASIBLE TABLE METHODS**
// -------------------------------
// *******************************

/**
 * 
 * @param {Table<BigRational>} table 
 * @param {(number | BigRational)[]} arr
 * @returns 
 */
function getMinBelowZero(table, arr) {
  let minIndex = NOT_FOUND;
  let min = Infinity;
  for (let i = 0; i < arr.length; i++) {
    const elem = arr[i];
    if (lesser(elem, r(0)) && lesser(elem, min)) {
      // @ts-ignore
      min = elem;
      minIndex = i;
    }
  }
  return minIndex;
}

/**
 * 
 * @param {Table<BigRational>} table 
 * @param {number} rowIndex 
 * @returns {number | NOT_FOUND}
 */
function getColumnIndex(table, rowIndex) {
  let min = Infinity;
  let minIndex = NOT_FOUND;
  let currentRow = table.rows[rowIndex];
  let lastRow = table.rows[table.rows.length - 1];
  for (let j = 0; j < currentRow.length - 1; j++) {
    //               except last column
    let elem = currentRow[j];
    // greater(lesser) работает не только с bigrational, но и c +-Infinity
    if (lesser(elem, r(0))) { // a[rowIndex][j] < 0
      // ratio = - lastRow / currentRow
      let ratio = lastRow[j].div(elem).negate();
      // console.log(`ratio=${ratio}`);
      if (lesser(ratio, min)) { // ratio < min
        // @ts-ignore
        min = ratio;
        minIndex = j;
      }
    }
  }
  return minIndex;
}

/**
 * 
 * @param {Table<BigRational>} table 
 * @returns {Table<BigRational>}
 */
function solveNotFeasibleTable(table) {
  // если решение отрицательное, то пока что ленимся и не решаем
  // get solution column
  let basicSolution = [];
  let Bindex = table.rows[0].length - 1;
  for (let i = 0; i < table.rows.length - 1/*кроме последнего*/; i++) {
    basicSolution.push(table.rows[i][Bindex]);
  }
  let rowIndex = getMinBelowZero(table, basicSolution);
  // на первой итерации rowIndex не может быть NOT_FOUND 
  // потому что отработал метод hasNegativeSolution
  if (rowIndex == NOT_FOUND) {
    table.solutionExist = solutionType.UNDEFINED;
    return table;
  }
  let columnIndex = getColumnIndex(table, rowIndex);
  if (columnIndex == NOT_FOUND) {
    table.solutionExist = solutionType.FALSE;
    return table;
  }
  // console.log(`x${table.basicOrder[rowIndex]} leave the base`);
  // console.log(`x${columnIndex} enters the base`);
  table = gaussElimination(table, rowIndex, columnIndex);
  table.basicOrder[rowIndex] = columnIndex;
  table.solutionExist = solutionType.NEGATIVE;
  return table;
}

/**
 * Find solutiion for given float table
 * @param {Table<BigRational>} table
 * @param {number} maxSteps
 * @returns {Solution<BigRational>} solution object `{f, solution}`
 */
function findSolution(table, maxSteps = MAX_STEPS) {
  table = setZeroObjectiveRowInBasicColumns(table);
  
  for (let step = 0; step < maxSteps; step++) {
    if (step >= 1) {
      // solve steps
      if (table.solutionExist == solutionType.UNDEFINED) {
        table = nextStep(table);
      } else
      if (table.solutionExist == solutionType.NEGATIVE) {
        table = solveNotFeasibleTable(table);
      }
    }
    const sType = table.solutionExist;
    if (!(sType === solutionType.UNDEFINED || sType === solutionType.NEGATIVE)) {
      break;
    }
  }
  if (table.solutionExist == solutionType.UNDEFINED) {
    console.warn(`MAX_STEPS = ${MAX_STEPS} not enough to find a solution`);
  }
  return getSolutionObject(table);
}

function testSolver() {
  /**
   * ### Когда задача имеет несколько оптимальных планов?
   * Если в индексной строке симплексной таблицы оптимального плана находится нуль, принадлежащий свободной переменной
   * не вошедшей в базис, а в столбце содержащем этот нуль имеется хотя бы один положительный элемент, то 
   * задача имеет множество оптимальных планов.
   * 
   * ### Получение соседнего оптимального плана
   * Свободную переменную, соответствующую указанному столбцу, можно внести в базис, выполнив соответствующие этапы алгоритма.
   * В результате будет получен второй оптимальный план с другим набором базисных переменных.
   */
  const floatTable = {
    basicOrder: [3, 4, 5], // порядок записи базисных переменных
    rows: [
      [-2,  1, -1, 1, 0, 0,-4],
      [ 1,  1,  2, 0, 1, 0, 8],
      [ 0, -1,  1, 0, 0, 1,-2],
      [+1, +2, +3, 0, 0, 0, 0], // objective row
    ],
    solutionExist: solutionType.UNDEFINED,
    isRational: false,
  };

  let rationalTable = tableToRational(floatTable);

  printRationalTable(rationalTable, DEBUG_RATIONAL_DIGITS);
  rationalTable = setZeroObjectiveRowInBasicColumns(rationalTable);
  
  for (let step = 0; step < MAX_STEPS; step++) {
    console.log(`---- STEP: ${step}) ----`);
    if (step >= 1) {
      // solve steps
      if (rationalTable.solutionExist == solutionType.UNDEFINED) {
        rationalTable = nextStep(rationalTable);
      } else
        if (rationalTable.solutionExist == solutionType.NEGATIVE) {
          rationalTable = solveNotFeasibleTable(rationalTable);
        }
    }
    // debug information
    printRationalTable(rationalTable, DEBUG_RATIONAL_DIGITS);
    if (rationalTable.solutionExist == solutionType.UNDEFINED) {
      console.log('\nСurrent base plan:');
      printSolutionFromTable(rationalTable);
      console.log();
    } else
    if (rationalTable.solutionExist == solutionType.NOT_BOUND) {
      console.log("Objective function don't have upper bound");
      printSolutionFromTable(rationalTable);
      break;
    } else
    if (rationalTable.solutionExist == solutionType.TRUE) {
      console.log('Solution found.');
      console.log('Optimal base plan:');
      printSolutionFromTable(rationalTable);
      break;
    } else
    if (rationalTable.solutionExist == solutionType.FALSE) {
      console.log('No solutions found.');
      break;
    } else
    if (rationalTable.solutionExist == solutionType.NEGATIVE) {
      console.log(`Found negative solution.\n`);
      console.log('Сurrent base plan:');
      printSolutionFromTable(rationalTable);
    }
  }
  if (rationalTable.solutionExist == solutionType.UNDEFINED) {
    console.log(`MAX_STEPS = ${MAX_STEPS} not enough to find a solution`);
  }  
}

module.exports = {
  testSolver, findSolution, printSolution, printSolutionFromTable
};