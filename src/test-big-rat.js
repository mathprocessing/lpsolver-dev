const bigInt = require('big-integer');
const { BigRational, r } = require('./my-big-rat');
const { assert, CC } = require('./utils');

let assertionErrorCount = 0;

/**
 * @template T
 * @param {T} value1
 * @param {T} value2
 * @param {string} message 
 */
function assertEq(value1, value2, message = "Assertion failed.") {
  console.assert(value1 === value2, message);
  if (value1 !== value2) {
    assertionErrorCount++;
  }
  console.log('ok:', value1, 'equals to', value2);
}

function testAdd() {
  console.log("\ntestAdd:");
  const x = r(2, 3);
  const y = r(1, 6);
  
  assertEq(x.add(y).toString(), '5/6');
}

function testMul() {
  console.log("\ntestMul:");
  let x = r(2, 3);
  let y = r(1, 6);
  assertEq(x.mul(y).toString(), '1/9');

  x = r(-2, 3);
  y = r(1, -6);
  assertEq(x.mul(y).toString(), '1/9');
}

function testDiv() {
  console.log("\ntestDiv:");
  const pairs = [
    [100000, 99998], // powers of two 1.000020000400008000160003200064001280025600512010240204804096
    [7, 4],
    [4, 7],
    [7, 7],
    [1, 1],
    [300, 5],
    [300, 39],
    [100000000, 3],
    [343, -35],
    [707, 7],
    [707, 3],
    [1000, 999],
    [1000000, 1],
    [11, -2],
    [-2, -11],
    [-2, 11],
  ];

  const enoughDigits = 8;

  /** @param {string} s */
  function haveDecimalPart(s) {
    return s.indexOf('.') !== -1;
  }

  /** @param {string} s */
  function trimZeros(s) {
    while (s.length > 0 && s[s.length - 1] === "0") {
      s = s.slice(0, -1);
    }
    return s;
  }

  for (let [n, d] of pairs) {
    let s = (n / d).toString();
    if (s.length > enoughDigits && haveDecimalPart(s)) {
      // prevent possible float errors in last digit
      s = s.slice(0, -1);
    } 
    if (s[s.length - 1] === ".") {
      s = s.slice(0, -1);
    }
    s = trimZeros(s);
    // console.log(s);
    const splitS = s.split('.');
    const fractionalPartLength = (splitS.length === 2) ? splitS[1].length : 0;

    const bRat = trimZeros(r(n, d).toDecimal(Math.max(fractionalPartLength, enoughDigits)));
    // console.log(splitS, fractionalPartLength,);
    assertEq(bRat, s, `bigrat: (${bRat}) not equal to float: (${s})`);
  }
}

function test_rfunc() {
  const pairs = [
    [38374864, 46346437],
    [-3, 5],
    [3, -5],
    [-3, -5],
    [-3, -5],
    [0, 1],
    [1, 1],
    [-1, 1],
    [-100, 1],
  ]
  for (let [n, d] of pairs) {
    const value = r(n, d);
    const vnum = value.num;
    const vdenom = value.denom;
    const bigRat = BigRational.reduce(bigInt(n), bigInt(d));
    assert(vnum.eq(bigRat.num) && vdenom.eq(bigRat.denom),
      CC.setYellow(`actual: [${vnum}, ${vdenom}], expected: [${n}, ${d}]`));
  }
}

function test_rfunc_float() {
  const pairs = [
    { input: 2.5, output: "5/2"},
    { input: -2.5, output: "-5/2"},
    { input: -3, output: "-3"},
    { input: 5, output: "5"},
    { input: 1, output: "1"},
    { input: 0, output: "0"},
    { input: 0.001, output: "1/1000"},
    { input: 0.000000000001, output: "1/1000000000000"},
    { input: -0.000000000001, output: "-1/1000000000000"},
    { input: 0.333, output: "333/1000"},
    { input: -0.333, output: "-333/1000"},                           
    // { input: -53532985935.3535763753, output: "-535329859353535763753/10000000000"},
    { input: -53532985935.31, output: "-5353298593531/100"},
    // { input: -53532985935.310001, output: "-53532985935310001/10000"},
    // 16 digits float -> Ok
    { input: -53532985935.31559, output: "-5353298593531559/100000"},
    // 17 digits float -> Error happens
    { input: -53532985935.315593, output: "-5353298593531559/100000"},
  ];
  for (let {input, output} of pairs) {
    const left = r(input).toString();
    const right = output;
    assert(left === right, CC.setYellow(`\n\n from input: ${left}, expected output: ${right}`));
  }
}

testMul();
testAdd();
testDiv();
test_rfunc();
test_rfunc_float();

if (assertionErrorCount > 0) {
  throw new Error(`Number of assertion Errors: ${assertionErrorCount}`);
} else {
  console.log(CC.setGreen("No assertion errors."));
}

/*
todo: fix r function
console.log(r(3.5, 3.4).toString());
// 411764705882353/400000000000000 != 119/10
console.log(r(35, 10).mul(r(34, 10)).toString());
// 119/10
*/