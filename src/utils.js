const { r } = require("./my-big-rat");

const {
  solutionType,
  NOT_FOUND,
} = require('./consts');
const { BigRational } = require("./my-big-rat");

/**
 * Console colors.
 */
const CC = {
  black: "30",
  red: "31m",
  green: "32",
  yellow: "33",
  blue: "34",
  magenta: "35",
  cyan: "36",
  white: "37",
  /**
   * Set console color for string `s`.
   * 
   * Example:
   * ```
   * CC.setColor("hello", CC.green);
   * // or
   * CC.setGreen("hello");
   * ```
   * @param {string} s 
   * @param {string} consoleColorString
   * @returns {string} `"\x1b...\x1b[0m"`
   */
  _setColor: (s, consoleColorString) => `\x1b[${consoleColorString}m${s}\x1b[0m`,
  /** @param {string} s */
  setBlack: s => CC._setColor(s, CC.black),
  /** @param {string} s */
  setRed: s => CC._setColor(s, CC.red),
  /** @param {string} s */
  setGreen: s => CC._setColor(s, CC.green),
  /** @param {string} s */
  setYellow: s => CC._setColor(s, CC.yellow),
  /** @param {string} s */
  setBlue: s => CC._setColor(s, CC.blue),
  /** @param {string} s */
  setMagenta: s => CC._setColor(s, CC.magenta),
  /** @param {string} s */
  setCyan: s => CC._setColor(s, CC.cyan),
  /** @param {string} s */
  setWhite: s => CC._setColor(s, CC.white),
}

/**
 * Assert: if value is false ==> throws `Assertion Error`
 * @param {boolean} value
 * @param  {...any} args
 */
function assert(value, ...args) {
  if (!value) {
    console.assert(false, ...args);
    throw new Error(`Assertion Error`);
  }
}

/**
 * 
 * @param {number} solutionExist 
 * @returns {string}
 */
function solutionExistingToString(solutionExist) {
  if (typeof solutionExist === "number") {
    //@ts-ignore
    return solutionType[solutionExist];
  } else {
    throw new Error("input muse be number");
  }
}

/**
 * Copy array if it elements have `.copy()` method
 * @param {Copyable[]} inputArray
 * @returns {Copyable[]} copy of array
 */
function copyArray(inputArray) {
  let result = [];
  for (let i = 0; i < inputArray.length; i++) {
    result.push(inputArray[i].copy());
  }
  return result;
}

/**
 * @template T
 * @param {Table<Copyable>} table 
 * @returns {Table<Copyable>}
 */
function copyTable(table) {
  let newTable = {};
  // Copy rows
  let newRowsMatrix = [];
  for (let i = 0; i < table.rows.length; i++) {
    let row = table.rows[i];
    let newRow = [];
    for (let j = 0; j < row.length; j++) {
      newRow.push(row[j].copy());
    }
    newRowsMatrix.push(newRow);
  }
  newTable.rows = newRowsMatrix;
  // newTable.objective = copyArray(table.objective);
  newTable.basicOrder = table.basicOrder.slice();
  newTable.solutionExist = table.solutionExist;
  newTable.isRational = table.isRational;
  return newTable;
}

/**
 * Calculate solution vector (plan) from given table
 * @template T
 * @param {Table<T>} table 
 * @returns {T[]}
 */
function getSolutionVector(table) {
  let solution = [];
  let lastElementIndex = table.rows[0].length - 1;
  for (let i = 0; i < lastElementIndex; i++) {
    let elemIndex = table.basicOrder.indexOf(i);
    if (elemIndex === NOT_FOUND) {
      // отличие от getSolution: solution.push(0)
      if (isRationalTable(table) === true) {
        solution.push(r(0));
      } else {
        solution.push(0);
      }
    } else {
      let value = table.rows[elemIndex][lastElementIndex];
      solution.push(value);
    }
  }
  //@ts-ignore
  return solution;
}

/**
 * Check that given table contains matrix of rational numbers
 * @param {Table<any>} table 
 * @returns {boolean}
 */
function isRationalTable(table) {
  checkTableRationalProperty(table);
  return table.isRational;
}

/**
 * @param {Table<any>} table 
 */
function checkTableRationalProperty(table) {
  if (table.isRational == undefined) {
    throw new Error('table.isRational must be defined');
  }
}

module.exports = {
  solutionExistingToString,
  copyTable,
  getSolutionVector,
  assert,
  CC
};


// function getMax(numArray) {
  // return Math.max.apply(null, numArray);
// }

// function getMin(numArray) {
  // return Math.min.apply(null, numArray);
// }

// function argMax(array) {
  // return array.map((x, i) => [x, i]).reduce((r, a) => (a[0] > r[0] ? a : r))[1];
// }