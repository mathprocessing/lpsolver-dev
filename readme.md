# About
This code can be answer to questions:
* How to solve linear programming tasks with arbitrary precision?
* How to use rational number libraries to make this working?
* How to prove that library do not have bugs? (Hard question)

# Dev
if installed: 
* node
* nodemon

run
```
npm start
```

# Test
```
npm test
```

# Dev state
Now only `integer-solver.js` works without errors

# Generate type definitions
https://www.typescriptlang.org/docs/handbook/declaration-files/dts-from-js.html
```
$ tsc --version
Version 4.2.4
$ tsc src\my-big-rat\index.js --declaration --allowJs --emitDeclarationOnly --outDir types
```

# Colors reference
```
Reset = "\x1b[0m"
Bright = "\x1b[1m"
Dim = "\x1b[2m"
Underscore = "\x1b[4m"
Blink = "\x1b[5m"
Reverse = "\x1b[7m"
Hidden = "\x1b[8m"

FgBlack = "\x1b[30m"
FgRed = "\x1b[31m"
FgGreen = "\x1b[32m"
FgYellow = "\x1b[33m"
FgBlue = "\x1b[34m"
FgMagenta = "\x1b[35m"
FgCyan = "\x1b[36m"
FgWhite = "\x1b[37m"

BgBlack = "\x1b[40m"
BgRed = "\x1b[41m"
BgGreen = "\x1b[42m"
BgYellow = "\x1b[43m"
BgBlue = "\x1b[44m"
BgMagenta = "\x1b[45m"
BgCyan = "\x1b[46m"
BgWhite = "\x1b[47m"
```

# TODO
`@todo implement nearestRational(x, dx, maxNumOrDenom)`