## Interesting idea of possible better imports resolving
This line must be the same in two distinct files.
```
const { assert } = require('./utils');
```
How to place this rule to some framework to check code existing
Possible solutions:
* Link to code `@assert-line(args)`
* Don't ivent new things and just use import statement

Result of solving:
* Automatic imports resolving (proving that some import combinations can't be optimal)
* imports can be throwable from some outside file
```
// outside.js
@add_import(@assert_line).to('@app/main.js');
```

### In addition
Also we can have same properties of `code files`.
For example:
```
if main.js imports from A.js setMain then utils.js must import from A.js setUtils

and setMain is subset of setUtils
```

## Idea of infinite ordered set of detalization levels
Example:
```js
/**
 * Add char to beginning of string `s` with goal to `s.length >= targetLength`
 * @param {string} s 
 * @param {number} targetLength
 * @returns string with `length >= targetLength:`
 * 
 * more delailed:
 * 
 * `if inputLength <= targetLength ==> s.length = targetLength`
 * 
 * `if inputLength > targetLength ==> s.length = inputLength`
```

Level of detalization can be numered:
```
Detalization level #0:
Identity object // some strange object that `do nothing` like in group theory

Detalization level #1:
@returns string with `length >= targetLength:`

Detalization level #2:
`if inputLength <= targetLength ==> s.length = targetLength`
`if inputLength > targetLength ==> s.length = inputLength`

Detalization level #3:
[Some more compicated math]

...
Detalization level #N:
...
```
