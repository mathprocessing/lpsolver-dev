const NOT_FOUND = -1
const DEBUG_DIGITS = 5
const r = require("big-rational")

let a = r(15)
let b = r(20, -1)

console.log(isBigRat(a))

let c = sumTwo(a, b)
console.log(`${a} + ${b} = ${c}`)

let ratArray = arrayToRational(["3/2", 5, -6n])
console.log(`sum(${rationalArrayToString(ratArray, 3)}) = ${sumArray(ratArray)}`)

ratArray = arrayToRational(["0.125", 7.5, -3n])
console.log(rationalArrayToString(ratArray))

function isBigRat(obj) {
  return typeof obj == 'object' && obj.hasOwnProperty('num')
}

function sumTwo(a, b) {
  if((isBigRat(a) && isBigRat(b)) == false) {
    throw new Error("object is not a big rational")
  }
  return a.add(b)
}

function sumArray(bigRatArray) {
  let rationalSum = r(0)
  for(let i = 0; i < bigRatArray.length; i++) {
    let rationalNumber = bigRatArray[i]
    if(isBigRat(rationalNumber) == false) {
      throw new Error("object is not a big rational")
    }
    rationalSum = rationalSum.add(rationalNumber)
  }
  return rationalSum
}

function rationalArrayToString(rArray, numberLength = DEBUG_DIGITS) {
  let s = '['
  for(let i = 0; i < rArray.length; i++) {
    if(i > 0) {
      s += ','
    }
    s += rationalToString(rArray[i], numberLength)
  }
  s += ']'
  return s
}

function arrayToRational(arr) {
  let rArray = []
  for(let i = 0; i < arr.length; i++) {
    rArray.push(r(arr[i]))
  }
  return rArray
}

function numberToString(x, numberLength = DEBUG_DIGITS) {
  let s = String(x)
  while(s.length < numberLength) {
    s = ' ' + s
  }
  return s
}

function rationalToString(rational, numberLength = DEBUG_DIGITS) {
  if(typeof rational == 'number') {
    return numberToString(rational, numberLength)
  } else
  if(rational.denominator.value === 1n) {
    return numberToString(rational.numerator.value, numberLength)
  } else {
    return numberToString(rational, numberLength)
  }
}