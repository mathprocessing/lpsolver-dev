const r = require("big-rational")
//---------------------------------
const DEBUG_DIGITS = 3
const MAX_STEPS = 8
//---------------------------------
const {
  solutionType,
  NOT_FOUND,
} = require('./consts')

if(NOT_FOUND == undefined) {
  throw new Error('NOT_FOUND is undefined')
}

const {
  solutionExistingToString,
  copyTable,
  getSolution,
} = require('./utils')

function printTable(table) {
  console.log('Table: {')
  console.log(`basicOrder:`, table.basicOrder)
  for(let i = 0; i < table.rows.length; i++) {
    console.log(`r${i}:`, arrayToString(table.rows[i]))
  }
  console.log(`solutionExist:`, solutionExistingToString(table.solutionExist), '\n}')
}

function arrayToString(row) {
  let s = '['
  for(let i = 0; i < row.length; i++) {
    if(i > 0) {
      s += ', '
    }
    s += row[i].toFixed(DEBUG_DIGITS)
  }
  s += ']'
  return s
}

function getMinBelowZeroExceptLast(arr) {
  let minIndex = NOT_FOUND, min = Infinity
  for(let i = 0; i < arr.length - 1; i++) {
    //               except last element
    let elem = arr[i]
    if(elem < 0 && elem < min) {
      min = elem
      minIndex = i
    }
  }
  return minIndex
}

function getRowIndex(rows, columnIndex, Bindex) {
  let min = Infinity
  let minIndex = NOT_FOUND
  for(let i = 0; i < rows.length - 1; i++) {
    //               except last row
    let row = rows[i]
    if(row[columnIndex] > 0) {
      let elem = row[Bindex] / row[columnIndex]
      if(elem < min) {
        min = elem
        minIndex = i
      }
    }
  }
  return minIndex
}

function hasNegativeSolution(table, Bindex) {
  for(let i = 0; i < table.rows.length - 1; i++) {
    // элемент в столбце B
    let solutionElement = table.rows[i][Bindex]

    if( solutionElement < 0 ) {
      return true
    }
  }
  return false
}

function nextStep(table) {
  let rows = table.rows
  let objectiveRow = rows[rows.length - 1]
  let Bindex = rows[0].length - 1
  let lastObjectiveElement = objectiveRow[Bindex]
  
  if( hasNegativeSolution(table, Bindex) ) {
    // получено отрицательное решение
    table.solutionExist = solutionType.NEGATIVE
    return table
  }
  
  let columnIndex = getMinBelowZeroExceptLast(objectiveRow)
  
  if(columnIndex == NOT_FOUND) {
    // lastObjectiveElement >= 0
    // solution founded => exit algorithm
    table.solutionExist = solutionType.TRUE
    return table
  }
  // columnIndex != NOT_FOUND
  let rowIndex = getRowIndex(rows, columnIndex, Bindex)

  if(rowIndex == NOT_FOUND) {
    table.solutionExist = solutionType.NOT_BOUND
    return table
  }
  
  console.log(`rowIndex = ${rowIndex}`)
  console.log(`columnIndex = ${columnIndex}`)
  console.log('q = ')
  for(let i = 0; i < rows.length - 1; i++) {
    if(rows[i][columnIndex] == 0) {
      console.log(`(${rows[i][Bindex]}) / (${rows[i][columnIndex]}) = +-Infinity`)
    } else {
      console.log(`(${rows[i][Bindex]}) / (${rows[i][columnIndex]}) = ${rows[i][Bindex]/rows[i][columnIndex]}`)
    }
  }
  console.log(`intersectElement = ${rows[rowIndex][columnIndex]}`)
  
  
  let newTable = gaussElimination(table, rowIndex, columnIndex)
  newTable.basicOrder[rowIndex] = columnIndex
  return newTable
}

function gaussElimination(table, rowIndex, columnIndex) {
  // метод прямоугольников
  let rows = table.rows
  let newTable = copyTable(table)
  // console.log(`rowIndex = ${rowIndex}`)
  let a1 = rows[rowIndex][columnIndex]
  // console.log(`\ngaussElimination: `)
  // console.log(`a1 = ${a1}`)
  if(a1 <= 0) {
    console.log(`a1 <= 0`)
  }
  if(a1 == 0) {
    throw new Error(`a1 = 0`)
  }
  // i = rowIndex, j = columnIndex
  for(let i = 0; i < rows.length/*all rows*/; i++) {
    let row = rows[i]
    // console.log()
    if(i == rowIndex) {
      // newRow = row * (1/a1)
      // console.log(`i == rowIndex`)
      for(let j = 0; j < row.length; j++) {
        let b2 = row[j]
        let newElement = b2 / a1
        newTable.rows[i][j] = newElement
        // console.log(`(r${i}, c${j}) = ${b2.toFixed(DEBUG_DIGITS)}/${a1.toFixed(DEBUG_DIGITS)} = ${newElement.toFixed(DEBUG_DIGITS)}`)
      }
    } else {
      // newRow = row - (coef * mainRow)
      let b1 = row[columnIndex]
      let coef = b1 / a1
      // console.log(`coef = ${b1.toFixed(DEBUG_DIGITS)}/${a1.toFixed(DEBUG_DIGITS)} = ${coef.toFixed(DEBUG_DIGITS)}`)
      
      for(let j = 0; j < row.length; j++) {
        let b2 = row[j]
        let a2 = rows[rowIndex][j]
        let newElement = b2 - coef * a2
        newTable.rows[i][j] = newElement
        // console.log(`(r${i}, c${j}) = ${b2.toFixed(DEBUG_DIGITS)} - (${coef.toFixed(DEBUG_DIGITS)}) * ${a2.toFixed(DEBUG_DIGITS)} = ${newElement.toFixed(DEBUG_DIGITS)}`)
      }
    }
  }
  return newTable
}

function printSolutionFromTable(table) {
  let solution = getSolution(table)
  console.log(arrayToString(solution))
  let objectiveRow = table.rows[table.rows.length - 1]
  let func = objectiveRow[objectiveRow.length - 1]
  console.log(`func = ${func.toFixed(5)}`)
}

function setZeroObjectiveRowInBasicColumns(table) {
  console.log(`setZeroObjectiveRowInBasicColumns:`)
  let objectiveRow = table.rows[table.rows.length - 1]
  for(let i = 0; i < table.basicOrder.length; i++) {
    let varIndex = table.basicOrder[i]
    if(true/* objectiveRow[varIndex] != 0 */) {
      table = gaussElimination(table, i, varIndex)
    }
  }
  return table
}

// ***NOT FEASIBLE TABLE METHODS**
// -------------------------------
// *******************************

function getMinBelowZero(table, arr) {
  let minIndex = NOT_FOUND, min = Infinity
  for(let i = 0; i < arr.length; i++) {          
    let elem = arr[i] // Number
    if(elem < 0 && elem < min) {
      min = elem // тип Number
      minIndex = i // тип Number
    }
  }
  return minIndex // тип Number
}

function getColumnIndex(table, rowIndex) {
  let min = Infinity, minIndex = NOT_FOUND
  let currentRow = table.rows[rowIndex]
  let lastRow = table.rows[table.rows.length - 1]
  for(let j = 0; j < currentRow.length - 1; j++) {
    //               except last column
    let elem = currentRow[j]
    if( elem < 0 ) {
      // ratio = - lastRow / currentRow
      let ratio = - lastRow[j] / elem
      console.log(`ratio=${ratio}`)
      if( ratio < min ) { // ratio < min
        min = ratio // number
        minIndex = j // number
      }
    }
  }
  return minIndex // number or NOT_FOUND
}

function solveNotFeasibleTable(table) {
  // если решение отрицательное, то пока что ленимся и не решаем
  // get solution column
  let basicSolution = []
  let Bindex = table.rows[0].length - 1
  for(let i = 0; i < table.rows.length - 1/*кроме последнего*/; i++) {
    basicSolution.push(table.rows[i][Bindex])
  }
  let rowIndex = getMinBelowZero(table, basicSolution)
  // на первой итерации rowIndex не может быть NOT_FOUND 
  // потому что отработал метод hasNegativeSolution
  if(rowIndex == NOT_FOUND) {
    table.solutionExist = solutionType.UNDEFINED
    return table
  }
  let columnIndex = getColumnIndex(table, rowIndex)
  // console.log(`rowIndex = ${rowIndex}`)
  // console.log(`columnIndex = ${columnIndex}`)
  if(columnIndex == NOT_FOUND) {
    table.solutionExist = solutionType.FALSE
    return table
  }
  console.log(`x${table.basicOrder[rowIndex]} выходит из базиса(leave the base)`)
  console.log(`x${columnIndex} входит в базис(enters the base)`)
  table = gaussElimination(table, rowIndex, columnIndex)
  table.basicOrder[rowIndex] = columnIndex
  table.solutionExist = solutionType.NEGATIVE
  return table
}

console.log(4.9999999999999999)
console.log(4.999999999999999)

table = {
  basicOrder: [3, 4, 5], // порядок записи базисных переменных
  rows: [
    [-2,  1, -1, 1, 0, 0, -4],
    [ 1,  1,  2, 0, 1, 0,  8],
    [ 0, -1,  1, 0, 0, 1, -2],
    [+1, +2, +3, 0, 0, 0,  0], // objective
  ],
  solutionExist: solutionType.UNDEFINED,
  isRational: false,
}

printTable(table)
table = setZeroObjectiveRowInBasicColumns(table)
for(let step = 0; step < MAX_STEPS; step++) {
  // printSolution -> printRationalTable
  console.log(`---- ШАГ: ${step}) ----`)
  if(step >= 1) {
    // solve steps
    if(table.solutionExist == solutionType.UNDEFINED) {
      table = nextStep(table)
    } else
    if(table.solutionExist == solutionType.NEGATIVE) {
      table = solveNotFeasibleTable(table)
    }
  }
  // debug information
  printTable(table)
  if(table.solutionExist == solutionType.UNDEFINED) {
    console.log()
    console.log('текущий опорный план:')
    printSolutionFromTable(table)
    console.log()
  } else
  if(table.solutionExist == solutionType.NOT_BOUND) {
    console.log('целевая функция неограничена сверху')
    printSolutionFromTable(table)
    break
  } else
  if(table.solutionExist == solutionType.TRUE) {
    console.log('решение найдено')
    console.log('оптимальный опорный план:')
    printSolutionFromTable(table)
    break
  } else
  if(table.solutionExist == solutionType.FALSE) {
    console.log('РЕШЕНИЙ НЕТ')
    break
  } else
  if(table.solutionExist == solutionType.NEGATIVE) {
    console.log(`найдено отрицательное решение`)
    console.log()
    console.log('текущий опорный план:')
    printSolutionFromTable(table)
  }
}
if(table.solutionExist == solutionType.UNDEFINED) {
  console.log(`MAX_STEPS = ${MAX_STEPS} недостаточно для решения`)
}

