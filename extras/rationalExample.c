//https://possiblywrong.wordpress.com/2019/01/09/identical-packs-of-skittles/
#include "math_Rational.h"
#include <map>
#include <iostream>
using namespace math;
 
// Map powers of x to coefficients.
typedef std::map<int, Rational> Poly;
 
// Return f(x)*g(x) mod x^m.
Poly product_mod(Poly f, Poly g, int m)
{
    Poly h;
    for (int k = 0; k < m; ++k)
    {
        for (int j = 0; j <= k; ++j)
        {
            h[k] += f[j] * g[k - j];
        }
    }
    return h;
}
 
Rational p(int n, int d)
{
    // Compute g(x) = 1 + ... + (x^n/n!)^2.
    Poly g; g[0] = 1;
    Rational factorial = 1, d_2n = 1;
    for (int k = 1; k <= n; ++k)
    {
        factorial *= k;
        d_2n *= (d * d);
        g[2 * k] = 1 / (factorial * factorial);
    }
 
    // Compute f(x) = g(x)^d mod x^(2n+1).
    Poly f; f[0] = 1;
    for (int k = 0; k < d; ++k)
    {
        f = product_mod(f, g, 2 * n + 1);
    }
 
    // Return [x^(2n)]f(x) * (n!)^2 / d^(2n).
    return f[2 * n] * (factorial * factorial) / d_2n;
}
 
int main()
{
    std::cout << p(60, 5) << std::endl;
}